$(document).foundation();

$(document).ready(function() {
  //
  // SVG for Everybody adds SVG External Content support to all browsers.
  // https://github.com/jonathantneal/svg4everybody
  //
  svg4everybody();
});