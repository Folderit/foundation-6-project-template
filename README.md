Directory structure
-------------------

```
assets/              all assets that should go to live application
  css/
  img/
  js/
  svg/
proto/               source code of scss, js, kit and etc. This directory will never be published
  bower_components/
  js/
  kit/
  scss/
  tmp/               temp images that are for illustration purposes in prototype
```


#### CodeKit -- https://codekitapp.com
We are using CodeKit to handle all of the preprocessing workflow.
Just drag and drop this folder to CodeKit window and all the settings will be loaded automatically.

#### Compass -- http://compass-style.org
We use Compass for image sprites.

#### Some information on languages/tools used

##### Bower
We use Bower to handle 3rd party code (CodeKit's Assets function). Everything is saved in: `proto/bower_components/`

##### SASS
Custom SASS source is located in: `proto/scss/`
Don't use browser prefixes or browser specific hacks on code written. We are using [Autoprefixer](https://autoprefixer.github.io) on generated CSS file to handle all that. You can set Autoprefixer browser string in CodeKit project preferences: `Tools -> Autoprefixer`

* `app.scss` -- only SASS file that creates output. Acts as a bootstrap file for linking together all needed 3rd party and own custom SASS. Be sure to include only needed Foundation modules here to keep output CSS as small as possible
* `_mixins.scss` -- custom SASS mixins
* `_sprite.scss` -- handles Compass image sprites
* `_layout.scss` -- everything that has to do with overall layout
* `_elements.scss` -- code for specific pages/elements. Feel free to split this up more as needed


##### JavaScript
Custom JS source is located in: `proto/js/`
All the 3rd party JS code should be included in CodeKit on file: `proto/js/app.js`. Have a look of 'Linked files' tab on the files properties.

#### Kit
For HTML prototypes we use [KIT language](https://codekitapp.com/help/kit/) -- which is just HTML with some extra tags. Main use is to include repetitive HTML on pages so maintaining the prototype will not become a nightmare. 

#### SVG sprite
SVG icons are stored in: `assets/svg/`
If needed the icons can be used from there but preferred way is to use SVG sprite.
Useful webapp to generate SVG sprite: [Icomoon.io](https://icomoon.io/app/)
